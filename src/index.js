import { App } from '@slack/bolt'
import dotenv from 'dotenv'
import data from './quotes.json'

dotenv.config()

const app = new App({
  signingSecret: process.env.CLIENT_SIGNING_SECRET,
  token: process.env.BOT_TOKEN,
})

const findRandomQuote = (message) => {
  const result = data.filter((quote) => quote.keywords.some((keyword) => message.text.includes(keyword)))
  if (result.length === 0) return null
  return result[Math.floor(result.length * Math.random())]
}

const buildResponse = (quote) => {
  const lines = quote.text.split('\n')
  const maxLength = lines.reduce((res, line) => Math.max(res, line.length), 0)
  return `
${lines.map((line) => `> _${line.trim()}_`).join('\n')}
> ${Array(maxLength).fill(' ').join('')}- _*${quote.author || 'un petit rigolo'}*_`
}

// Reverse all messages the app can hear
app.message(({ message, say }) => {
  const quote = findRandomQuote(message)
  if (quote) {
    return say(buildResponse(quote))
  }
  return Promise.resolve()
})

export default (async () => {
  // Start the app
  await app.start(process.env.PORT ?? 3000)
  console.log('⚡️ Zenikote is running!')
})()
