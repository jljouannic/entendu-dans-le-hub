const prettierConfig = require('./.prettierrc.cjs')

module.exports = {
  root: true,
  plugins: ['prettier'],
  extends: ['airbnb-base', 'prettier'],
  rules: {
    'no-irregular-whitespace': ['error', { skipStrings: true, skipTemplates: true }],
    'prettier/prettier': ['warn', prettierConfig],
    'import/extensions': ['error', 'always'],
    'import/prefer-default-export': 'off',
  },
  parserOptions: {
    ecmaVersion: 2020,
    sourceType: 'module',
  },
  overrides: [
    {
      files: ['*.spec.js'],
      plugins: ['jest'],
      env: { 'jest/globals': true },
    },
  ],
}
